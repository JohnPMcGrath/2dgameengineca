/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var
COLS = 26,
ROWS = 26,
EMPTY = 0,
HOMIE = 1,
NUTS = 2,
LEFT  = 0,
UP    = 1,
RIGHT = 2,
DOWN  = 3,
KEY_LEFT  = 37,
KEY_UP    = 38,
KEY_RIGHT = 39,
KEY_DOWN  = 40,

canvas,	  
ctx,	  
keystate, 
frames,
speed,
score,
fpsElement;


var snake = function() {

this.fpsElement = document.getElementById('fps');
this.sound = new Sound("Kermit-Family-Guy-Yay.mp3");


//Images
this.background = new Image();
this.runnerImage = new Image();

//Background width and height
this.BACKGROUND_WIDTH = 1102;
this.BACKGROUND_HEIGHT = 400;

//Loading screen
this.loadingElement = document.getElementById('loading'),
this.loadingTitleElement = document.getElementById('loading-title'),

//Toast
this.toastElement = document.getElementById('toast'),

//Instructions
this.instructionsElement = document.getElementById('instructions'),

//States
this.paused = false,
this.PAUSED_CHECK_INTERVAL = 200,
this.windowHasFocus = true,
this.countdownInProgress = false,

//Time
this.lastAnimationFrameTime = 0,
this.fps = 60,
this.lastFpsUpdateTime = 0,
this.pauseStartTime,

//Sound
this.getSound = document.getElementById('get-sound');
        
        
        
        
        
        
        
        
        
        
        
        
        
/* var bgReady = false;
        var bgImage = new Image();
        bgImage.onload = function () {
            bgReady = true;
        };
        bgImage.src = "images/background.png"; */

/*sound = {
    
        init: function()
        {
           sound = new Sound("Kermit-Family-Guy-Yay.mp3");  
        }
}*/

}

grid = {
	width: null,  
	height: null, 
	_grid: null,  
	
	init: function(d, c, r) {
		this.width = c;
		this.height = r;
		this._grid = [];
		for (var x=0; x < c; x++) {
			this._grid.push([]);
			for (var y=0; y < r; y++) {
				this._grid[x].push(d);
			}
		}
	},

	set: function(val, x, y) {
		this._grid[x][y] = val;
	},

	get: function(x, y) {
		return this._grid[x][y];
	}
}

homie = {
	direction: null,
	last: null,	
				
	_queue: null,	 

	init: function(d, x, y) {
		this.direction = d;
		this._queue = [];
		this.insert(x, y);
	},

	insert: function(x, y) {

		this._queue.unshift({x:x, y:y});
		this.last = this._queue[0];
	},

	remove: function() {
		
		return this._queue.pop();
	}
};

function setFood() {
	var empty = [];

	for (var x=0; x < grid.width; x++) {
		for (var y=0; y < grid.height; y++) {
			if (grid.get(x, y) === EMPTY) {
				empty.push({x:x, y:y});
			}
		}
	}

        for(var i = 0; i < 5; i++){
	var randpos = empty[Math.round(Math.random()*(empty.length - 1))];
	grid.set(NUTS, randpos.x, randpos.y);
        }      
        

}

function main() {
	
        
        
	canvas = document.createElement("canvas");
	canvas.width = COLS*20;
	canvas.height = ROWS*20;
        
        var background = new Image();
        background.src = "../Images/background.png";
        
        background.onload = function()
        {
            ctx.drawImage(background, 0, 0);
        }
        
	ctx = canvas.getContext("2d");
	
	document.body.appendChild(canvas);
        
        
	
	ctx.font = "41px Helvetica";
	frames = 0;
	keystate = {};
	
	document.addEventListener("keydown", function(evt) {
		keystate[evt.keyCode] = true;
	});
	document.addEventListener("keyup", function(evt) {
		delete keystate[evt.keyCode];
	});
	
	init();
	loop();
}

function init() {
	score = 0;
        speed = 8;
        
        
        
	grid.init(EMPTY, COLS, ROWS);
	var sp = {x:Math.floor(COLS/2), y:ROWS-1};
	homie.init(UP, sp.x, sp.y);
	grid.set(HOMIE, sp.x, sp.y);
	setFood();
}

function loop() {
	update();
	draw();

	window.requestAnimationFrame(loop, canvas);
}

function update() {
	frames++;
        

	if (keystate[KEY_LEFT] && homie.direction !== RIGHT) {
		homie.direction = LEFT;
	}
	if (keystate[KEY_UP] && homie.direction !== DOWN) {
		homie.direction = UP;
	}
	if (keystate[KEY_RIGHT] && homie.direction !== LEFT) {
		homie.direction = RIGHT;
	}
	if (keystate[KEY_DOWN] && homie.direction !== UP) {
		homie.direction = DOWN;
	}
	
	if (frames%speed === 0) {

		var nx = homie.last.x;
		var ny = homie.last.y;
		
		switch (homie.direction) {
			case LEFT:
				nx--;
				break;
			case UP:
				ny--;
				break;
			case RIGHT:
				nx++;
				break;
			case DOWN:
				ny++;
				break;
		}
		
		if (0 > nx || nx > grid.width-1  ||
			0 > ny || ny > grid.height-1 ||
			grid.get(nx, ny) === HOMIE
		) {
                       // sound.play();
			return init();
                        
		}
		
		if (grid.get(nx, ny) === NUTS) {
                    
                    score++;
                   //this.sound.play;
			
                        if((score % 5) === 0)
                        {
			setFood();
                    } 
                    
                       if((score % 10) === 0)
                        {
			speed--;
                    } 
                    
                    
                      
		} else {

			var tail = homie.remove();
			grid.set(EMPTY, tail.x, tail.y);
		}

		grid.set(HOMIE, nx, ny);
		homie.insert(nx, ny);
	}
}

function draw() {

	var tw = canvas.width/grid.width;
	var th = canvas.height/grid.height;
        
      //  if (bgReady) {
	//	ctx.drawImage(bgImage, 0, 0);
	//}
	
	for (var x=0; x < grid.width; x++) {
		for (var y=0; y < grid.height; y++) {
			
	
			switch (grid.get(x, y)) {
				case EMPTY:
					ctx.fillStyle = "#fff";
					break;
				case HOMIE:
					ctx.fillStyle = "#0ff";
					break;
				case NUTS:
					ctx.fillStyle = "#f00";
					break;
			}
			ctx.fillRect(x*tw, y*th, tw, th);
		}
	}

	ctx.fillStyle = "#000";
	ctx.fillText("SCORE: " + score, 10, canvas.height-10);
       
}

main();

